/*
 * uart_wrapper.h
 *
 *  Created on: Feb 16, 2019
 *      Author: reldn_000
 */

#ifndef UART_WRAPPER_H_
#define UART_WRAPPER_H_

#include <stdint.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif

bool uart3_init(unsigned int baudRate, int rxQSize, int txQSize);

void uart3_setBaudRate(unsigned int baudRate);

bool uart3_getChar(char* pInputChar, unsigned int timeout);

bool uart3_putChar(char out, unsigned int timeout);

bool uart3_flush(void);

#ifdef __cplusplus
}
#endif


#endif /* UART_WRAPPER_H_ */
