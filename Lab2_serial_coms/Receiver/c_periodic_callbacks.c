/*
 * c_periodic_callbacks.c
 *
 *  Created on: Feb 16, 2019
 *      Author: reldn_000
 */

#include "c_periodic_callbacks.h"
#include <stdio.h>
#include <stdbool.h>
#include "uart_wrapper.h"
#include "LED_Display_wrapper.h"



bool c_period_init(void)
{
    bool uart_init_status = uart3_init(38400, 64, 64);
    bool LED_init_status = LED_wrap_init();
    return uart_init_status && LED_init_status;
}

bool c_period_reg_tlm(void)
{
    return true;
}



void c_period_1Hz(uint32_t count)
{
    //LED_wrap_setNumber(count);
    count++;
}
void c_period_10Hz(uint32_t count)
{
    count++;
}

void c_period_100Hz(uint32_t count)
{
    count++;
    char received_char = 0;
    if (uart3_getChar(&received_char, 0)){
        LED_wrap_setNumber(received_char);
    }
}
void c_period_1000Hz(uint32_t count)
{
    count++;
}
