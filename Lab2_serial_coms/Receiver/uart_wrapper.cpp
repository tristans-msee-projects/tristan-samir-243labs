/*
 * uart_wrapper.cpp
 *
 *  Created on: Feb 16, 2019
 *      Author: reldn_000
 */

#include "uart_wrapper.h"
#include "uart3.hpp"
#include "freeRTOS.h"



bool uart3_init(unsigned int baudRate, int rxQSize, int txQSize){
    return Uart3::getInstance().init(baudRate, rxQSize, txQSize);
}

void uart3_setBaudRate(unsigned int baudRate){
    Uart3::getInstance().setBaudRate(baudRate);
}

bool uart3_getChar(char* pInputChar, unsigned int timeout=portMAX_DELAY){
    return Uart3::getInstance().getChar(pInputChar, timeout);
}

bool uart3_putChar(char out, unsigned int timeout=portMAX_DELAY){
    return Uart3::getInstance().putChar(out, timeout);
}

bool uart3_flush(void){
    return Uart3::getInstance().flush();
}





