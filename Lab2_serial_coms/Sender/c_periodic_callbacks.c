/*
 * c_periodic_callbacks.c
 *
 *  Created on: Feb 12, 2019
 *      Author: csami
 */
//#include "FreeRTOS.h"
#include "c_periodic_callbacks.h"
#include <stdio.h>
#include "uart_wrapper.h"
#include "c_gpio.h"
#include <stdbool.h>
#include <stdint.h>

bool c_period_init(void)
{
    return uart2_init(38400, 64, 64);
}

bool c_period_reg_tlm(void)
{
    return true;
}

void c_period_1Hz(int count)
{
    count++;
}

void c_period_10Hz(int count, struct switchStatus_S * p_S)
{
    if (p_S->switchStatus_4) {
        uart2_putChar((4), 0);
    }
    else if (p_S->switchStatus_3) {
        uart2_putChar((3), 0);
    }
    else if (p_S->switchStatus_2) {
        uart2_putChar((2), 0);
    }
    else if (p_S->switchStatus_1) {
        uart2_putChar((1), 0);
    }
    else {
        uart2_putChar((0), 0);
    }
    count++;
}

void c_period_100Hz(int count, struct switchStatus_S * p_S)
{
    p_S->switchStatus_4 = read_a_switch(4);
    p_S->switchStatus_3 = read_a_switch(3);
    p_S->switchStatus_2 = read_a_switch(2);
    p_S->switchStatus_1 = read_a_switch(1);
    count++;
}

void c_period_1000Hz(int count)
{
    count++;
}
