/*
 * c_periodic_callbacks.h
 *
 *  Created on: Feb 12, 2019
 *      Author: csami
 */

#ifndef C_PERIODIC_CALLBACKS_H_
#define C_PERIODIC_CALLBACKS_H_

#ifdef __cplusplus
extern "C" {
#endif

//#include "periodic_callback.h"
#include <stdbool.h>
#include <stdio.h>

struct switchStatus_S
{
    bool switchStatus_1;
    bool switchStatus_2;
    bool switchStatus_3;
    bool switchStatus_4;
};


bool c_period_init(void);
bool c_period_reg_tlm(void);

void c_period_1Hz(int count);
void c_period_10Hz(int count, struct switchStatus_S * p_S);
void c_period_100Hz(int count, struct switchStatus_S * p_S);
void c_period_1000Hz(int count);

#ifdef __cplusplus
}
#endif

#endif /* C_PERIODIC_CALLBACKS_H_ */
