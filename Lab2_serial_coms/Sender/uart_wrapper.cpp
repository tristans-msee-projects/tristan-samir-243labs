// @file uart_wrapper.cpp
#include "uart_wrapper.h"
#include "uart2.hpp"
#include "stdbool.h"
#include "freeRTOS.h"


bool uart2_init(unsigned int baudRate, int rxQSize=32, int txQSize=64)
{
    return Uart2::getInstance().init(baudRate, rxQSize, txQSize);
}

bool uart2_getChar(char* pInputChar, unsigned int timeout=portMAX_DELAY)
{
    return Uart2::getInstance().getChar(pInputChar, timeout);
}

bool uart2_putChar(char out, unsigned int timeout=portMAX_DELAY)
{
    return Uart2::getInstance().putChar(out, timeout);
}


