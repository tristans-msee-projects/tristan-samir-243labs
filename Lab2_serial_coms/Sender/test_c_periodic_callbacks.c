/*
 * test_c_periodic_callbacks.c
 *
 *  Created on: Feb 16, 2019
 *      Author: csami
 */
#include "unity.h"
#include <stdbool.h>
#include <stdio.h>
#include "Mockuart_wrapper.h"
#include "c_periodic_callbacks.h"
#include <stdint.h>
#include "Mockc_gpio.h"

struct switchStatus_S S;

void setUp(void)
{
    S.switchStatus_1 = 1;
    S.switchStatus_2 = 1;
    S.switchStatus_3 = 1;
    S.switchStatus_4 = 1;
}

void tearDown(void)
{
}

void test_c_period_init(void)
{
    uart2_init_IgnoreAndReturn(true);
    TEST_ASSERT_TRUE(c_period_init());
}

void test_c_period_reg_tlm(void)
{
    c_period_reg_tlm();
}

void test_c_period_1Hz(void)
{
    c_period_1Hz(0);
}

void test_c_period_10Hz(void)
{
    uart2_putChar_ExpectAndReturn(4, 0, true);
    c_period_10Hz(0, &S);
    S.switchStatus_4 = 0;
    uart2_putChar_ExpectAndReturn(3, 0, true);
    c_period_10Hz(0, &S);
    S.switchStatus_3 = 0;
    uart2_putChar_ExpectAndReturn(2, 0, true);
    c_period_10Hz(0, &S);
    S.switchStatus_2 = 0;
    uart2_putChar_ExpectAndReturn(1, 0, true);
    c_period_10Hz(0, &S);
    S.switchStatus_1 = 0;
    uart2_putChar_ExpectAndReturn(0, 0, true);
    c_period_10Hz(0, &S);
}

void test_c_period_100Hz(void)
{
    read_a_switch_IgnoreAndReturn(0);
    read_a_switch_IgnoreAndReturn(0);
    read_a_switch_IgnoreAndReturn(0);
    read_a_switch_IgnoreAndReturn(0);
    c_period_100Hz(0, &S);
}

void test_c_period_1000Hz(void)
{
    c_period_1000Hz(0);
}

