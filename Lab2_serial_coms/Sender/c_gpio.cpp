/*
 * c_gpio.cpp
 *
 *  Created on: Feb 16, 2019
 *      Author: csami
 */
#include "c_gpio.h"
#include "io.hpp"


bool read_a_switch(int switch_num)
{
    return Switches::getInstance().getSwitch(switch_num);
}

