/*
 * c_gpio.h
 *
 *  Created on: Feb 16, 2019
 *      Author: csami
 */

#ifndef C_GPIO_H_
#define C_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdbool.h"

bool read_a_switch(int switch_num);

#ifdef __cplusplus
}
#endif

#endif /* C_GPIO_H_ */
