// @file uart_wrapper.h
#ifndef UART_WRAPPER_H__
#define UART_WRAPPER_H__


//#include "freeRTOS.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdio.h>

bool uart2_init(unsigned int baudRate, int rxQSize, int txQSize);
bool uart2_getChar(char* pInputChar, unsigned int timeout);
bool uart2_putChar(char out, unsigned int timeout);

#ifdef __cplusplus
}
#endif
#endif /* UART_WRAPPER_H__ */
